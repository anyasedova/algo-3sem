#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <assert.h>

void CountPalFunc(const std::string &s, std::vector<size_t> &PalFunc) {
    PalFunc.push_back(0);
    size_t l = 0;
    size_t r = 0;
    for (size_t i = 1; i < s.size(); i++) {
        PalFunc.push_back(r > i? std::min(r - i, PalFunc[l + r - i]) : 0);
        while ((i - PalFunc[i] > 0) && (i + PalFunc[i] + 1 < s.size())
               && (s[i + PalFunc[i] + 1] == s[i - PalFunc[i] - 1])) {
            PalFunc[i]++;
        }
        if (i + PalFunc[i] > r) {
            l = i - PalFunc[i];
            r = i + PalFunc[i];
        }
    }
}

void CountPalFuncEven(const std::string &s, std::vector<size_t> &PalFunc) {
    PalFunc.push_back(0);
    size_t l = 0;
    size_t r = 0;
    for (size_t i = 1; i < s.size(); i++) {
        PalFunc.push_back(r > i? std::min(r - i + 1, PalFunc[l + r - i + 1]) : 0);
        while ((i - PalFunc[i] > 0) && (i + PalFunc[i] < s.size())
               && (s[i + PalFunc[i]] == s[i - PalFunc[i] - 1])) {
            PalFunc[i]++;
        }
        if (i + PalFunc[i] > r) {
            l = i - PalFunc[i];
            r = i + PalFunc[i] - 1;
        }
    }
}

size_t CountPalindromNumber (std::string &s) {
    std::vector<size_t> PalFunc;
    std::vector<size_t> EvenPalFunc;
    CountPalFunc(s, PalFunc);
    CountPalFuncEven(s, EvenPalFunc);
    size_t res = 0;
    for (auto i: PalFunc){
        res += i;
    }
    for (auto i: EvenPalFunc) {
        res += i;
    }
    return res;
}
int main() {
    std::string s;
    std::cin >> s;
    std::cout << CountPalindromNumber(s);
    return 0;
}
