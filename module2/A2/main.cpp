#include <iostream>
#include <vector>
#include <assert.h>
#include <iomanip>
const double accuracy = 1e-9;

struct Vector {
    double x;
    double y;
    
    
    Vector(double x,double y) : x(x), y(y){}
    Vector(const Vector& vector) = default;
    
    void operator += (const Vector& other){
        x += other.x;
        y += other.y;
    }
    void operator -= (const Vector& other){
        x -= other.x;
        y -= other.y;
    }
    void operator /= (const double& c){
        assert(c > 0);
        x /= c;
        y /= c;
    }
    void operator *= (const double& c) {
        x *= c;
        y *= c;
    }
};

/*
 * Арифметические операции
 */
Vector operator + (const Vector& left, const Vector& right){
    Vector data = left;
    data += right;
    return data;
}

Vector operator - (const Vector& left, const Vector& right){
    Vector data = left;
    data -= right;
    return data;
}

Vector operator / (const Vector& left, const double& right){
    assert(right != 0);
    Vector data = left;
    data /= right;
    return data;
}

Vector operator * (const Vector& left, const double right){
    Vector data = left;
    data *= right;
    return data;
}

bool operator == (const Vector& left, const Vector& right) {
    return std::abs(left.x - right.x) <= accuracy && std::abs(left.y - right.y) <= accuracy;
}

bool operator != (const Vector& left, const Vector& right) {
    return !(left == right);
}



double OrientalSquare (const Vector& first, const Vector& second) {
    return first.x * second.y - first.y * second.x;
}


class Line {
public:
    
    friend bool isIntersect(Vector& x1, Vector& x2, Vector& y1, Vector& y2);
    
    Line(const Vector& first, const Vector& second) {
        a = first.y - second.y;
        b = second.x - first.x;
        c = first.x * second.y - second.x * first.y;
    }
    
    Vector getNormal() const {
        return Vector(a, b);
    }
    
    double getC() const {
        return c;
    }
    
    double getB() const {
        return b;
    }
    
    double getA() const {
        return a;
    }
    
private:
    double a;
    double b;
    double c;
};


bool isParallel(const Line& line1, const Line& line2) {
    return OrientalSquare(line1.getNormal(), line2.getNormal()) == 0;
}

bool isEqual(const Line& line1, const Line& line2) {
    return isParallel(line1, line2) && line1.getC() * line2.getB() == line2.getC() * line1.getB();
}

bool isBeside(const Vector& a, const Vector& b, const Vector& x) {
    if (a.x == b.x) {
        return (a.y <= x.y && x.y <= b.y) || (b.y <= x.y && x.y <= a.y);
    } else {
        return (a.x <= x.x && x.x <= b.x) || (b.x <= x.x && x.x <= a.x);
    }
}

bool isIntersect(Vector& x1, Vector& x2, Vector& y1, Vector& y2) {
    Line line1(x1, x2);
    Line line2(y1, y2);
    if (isEqual(line1, line2)) {
        return true;
    }
    if (isParallel(line1, line2)) {
        return false;
    }
    double c = line1.a * line2.b - line2.a * line1.b;
    
    Vector x(-(line1.c * line2.b - line2.c * line1.b),
             (line1.c * line2.a - line2.c * line1.a));
    
    return isBeside(x1 * c, x2 * c, x) && isBeside(y1 * c, y2 * c, x);
}


int main() {
    double x1, y1, x2, y2;
    std::cin >> x1 >> y1 >> x2 >> y2;
    Vector a1(x1, y1), a2(x2, y2);
    size_t n = 1;
    size_t numberOfBridges = 0;
    std::cin >> n;
    for (int i = 0; i < n; i++) {
        std::cin >> x1 >> y1 >> x2 >> y2;
        Vector r1(x1, y1), r2(x2, y2);
        if (isIntersect(a1, a2, r1, r2)){
            ++numberOfBridges;
        }
    }
    std::cout<< numberOfBridges;
    return 0;
}

