#include <iostream>
#include <cmath>
#include <vector>
#include <assert.h>
#include <iomanip>
#include <algorithm>

const long double pi = 3.141592653589793238462643;
const long double accuracy = 10e-9;

class Vector {
 public:
  long double x;
  long double y;

  Vector() = default;
  Vector(long double x, long double y) : x(x), y(y) {}
  Vector(const Vector& vector) = default;

  friend bool operator==(const Vector& left, const Vector& right);
  friend bool operator!=(const Vector& left, const Vector& right);
  friend Vector operator+(const Vector& left, const Vector& right);
  friend bool operator<(const Vector& left, const Vector& right);
  friend bool operator>(const Vector& left, const Vector& right);
  friend bool operator>=(const Vector& left, const Vector& right);
  friend bool operator<=(const Vector& left, const Vector& right);
  friend Vector operator-(const Vector& left, const Vector& right);
  friend Vector operator/(const Vector& left, const long double& right);

  void operator+=(const Vector& other) {
    x += other.x;
    y += other.y;
  }
  void operator-=(const Vector& other) {
    x -= other.x;
    y -= other.y;
  }
  void operator/=(const long double& c) {
    assert(c > 0);
    x /= c;
    y /= c;
  }
  void operator*=(const long double& c) {
    x *= c;
    y *= c;
  }
};

long double distance(const Vector& first, const Vector& second) {
  return sqrtl(((first.x - second.x) * (first.x - second.x) +
                (first.y - second.y) * (first.y - second.y)));
}

long double scalarMul(const Vector& first, const Vector& second) {
  return first.x * second.x + first.y * second.y;
}

/*
 * Арифметические операции
 */
bool operator<(const Vector& left, const Vector& right) {
  Vector zero(0, 0);
  if (right == left) {
    return false;
  }
  if (right == zero) {
    return false;
  }
  if (left == zero) {
    return true;
  }
  Vector a(0, -1);
  long double x = scalarMul(a, left) *
                      sqrtl(256 * (right.x * right.x + right.y * right.y)) / 16 -
                  scalarMul(a, right) *
                      sqrtl(256 * (left.x * left.x + left.y * left.y)) / 16;

  if (x == 0) {
    return left.x * left.x + left.y * left.y <
           right.x * right.x + right.y * right.y;
  } else {
    return x > -accuracy;
  }
}

bool operator<=(const Vector& left, const Vector& right) {
  return left < right || left == right;
}

bool operator>(const Vector& left, const Vector& right) {
  return !(left <= right);
}

bool operator>=(const Vector& left, const Vector& right) {
  return left > right;
}

Vector operator+(const Vector& left, const Vector& right) {
  Vector data = left;
  data += right;
  return data;
}

Vector operator-(const Vector& left, const Vector& right) {
  Vector data = left;
  data -= right;
  return data;
}

Vector operator/(const Vector& left, const long double& right) {
  assert(right != 0);
  Vector data = left;
  data /= right;
  return data;
}

Vector operator*(const Vector& left, const long double right) {
  Vector data = left;
  data *= right;
  return data;
}

bool operator==(const Vector& left, const Vector& right) {
  return fabsl(left.x - right.x) <= accuracy &&
         fabsl(left.y - right.y) <= accuracy;
}

bool operator!=(const Vector& left, const Vector& right) {
  return !(left == right);
}

long double OrientalSquare(const Vector& first, const Vector& second) {
  return first.x * second.y - first.y * second.x;
}

Vector FindMinVertice(std::vector<Vector>& vertices) {
  Vector min = vertices[0];
  for (auto& i : vertices) {
    if (i.x < min.x || (i.x == min.x && i.y < min.y)) {
      min = i;
    }
  }
  return min;
}

bool isOnTheSameSide(Vector& begin, Vector& end, Vector& p1, Vector& p2) {
  Vector side = begin - end;
  Vector v1 = p1 - end;
  Vector v2 = p2 - end;
  long double s1 = OrientalSquare(side, v1);
  long double s2 = OrientalSquare(side, v2);
  return ((s1 >= 0 && s2 >= 0) || (s1 <= 0 && s2 <= 0));
}

bool isOnTheSameLine(Vector& x, Vector& y, Vector& z) {
  Vector v1 = y - x, v2 = z - x;
  return OrientalSquare(v1, v2) == 0;
}

long double FindConvexHull(std::vector<Vector>& vertices) {
  long double res = 0;
  std::vector<Vector> vectors(vertices.size());
  std::vector<Vector> result;

  Vector min = FindMinVertice(vertices);

  for (auto& i : vertices) {
    vectors.push_back(i - min);
  }
  std::sort(vectors.begin(), vectors.end());

  Vector prev(min);
  result.push_back(prev);

  for (auto& i : vectors) {
    Vector nextVertice = min + i;

    if (nextVertice != prev) {
      if (result.size() < 2) {
        result.push_back(nextVertice);
      } else {
        while (result.size() > 1 &&
               isOnTheSameLine(min, result[result.size() - 1], nextVertice)) {
          result.pop_back();
        }

        size_t j = result.size() - 1;

        while (j > 0 &&
               (isOnTheSameLine(result[j], nextVertice, result[(j - 1)]) ||
                !isOnTheSameSide(result[j], nextVertice, result[(j - 1)],
                                 result[(j + 1) % result.size()]))) {
          --j;
        }

        for (size_t t = result.size() - 1; t > j; --t) {
          result.pop_back();
        }
        result.push_back(nextVertice);
      }
    }

    prev = nextVertice;
  }

  for (size_t i = 0; i < result.size(); i++) {
    res += distance(result[i], result[(i + 1) % result.size()]);
  }
  return res;
}

int main() {
  std::vector<Vector> vertices;
  size_t n;
  std::cin >> n;
  for (size_t i = 0; i < n; i++) {
    long double x, y;
    std::cin >> x >> y;
    vertices.emplace_back(x, y);
  }
  long double res = FindConvexHull(vertices);
  std::cout << std::setprecision(10) << res;
  return 0;
}
