#include <iostream>
#include <cmath>
#include <vector>
#include <assert.h>
#include <iomanip>
#include <algorithm>

const long double accuracy = 0.000000001;

class Vector {
 public:
  long double x;
  long double y;
  Vector() = default;
  Vector(long double x, long double y) : x(x), y(y) {}
  Vector(const Vector& vector) = default;

  void operator+=(const Vector& other) {
    x += other.x;
    y += other.y;
  }
  void operator-=(const Vector& other) {
    x -= other.x;
    y -= other.y;
  }
};
long double distance(const Vector& first, const Vector& second) {
  return sqrtl(((first.x - second.x) * (first.x - second.x) +
                (first.y - second.y) * (first.y - second.y)));
}

long double scalarMul(const Vector& first, const Vector& second) {
  return first.x * second.x + first.y * second.y;
}

/*
 * Арифметические операции
 */
bool operator==(const Vector& left, const Vector& right);
bool operator!=(const Vector& left, const Vector& right);
Vector operator+(const Vector& left, const Vector& right);
bool operator<(const Vector& left, const Vector& right);
bool operator>(const Vector& left, const Vector& right);
bool operator>=(const Vector& left, const Vector& right);
bool operator<=(const Vector& left, const Vector& right);
Vector operator-(const Vector& left, const Vector& right);
Vector operator/(const Vector& left, const long double& right);

bool operator<(const Vector& left, const Vector& right) {
  bool leftHalf = left.y < 0;
  bool rightHalf = right.y < 0;
  if (!leftHalf && rightHalf) {
    return true;
  }
  if (leftHalf && !rightHalf) {
    return false;
  }
  if (right == left) {
    return false;
  }
  if (right == Vector(0, 0)) {
    return false;
  }
  if (left == Vector(0, 0)) {
    return true;
  }
  Vector a(1, 0);
  long double x = scalarMul(a, left) *
                      sqrtl(256 * (right.x * right.x + right.y * right.y)) /
                      16 -
                  scalarMul(a, right) *
                      sqrtl(256 * (left.x * left.x + left.y * left.y)) / 16;
  if (fabsl(x) < accuracy) {
    return left.x * left.x + left.y * left.y <
           right.x * right.x + right.y * right.y;
  } else {
    if (!leftHalf && !rightHalf) {
      return x > -accuracy;
    } else {
      return x < -accuracy;
    }
  }
}

bool operator<=(const Vector& left, const Vector& right) {
  return left < right || left == right;
}

bool operator>(const Vector& left, const Vector& right) {
  return !(left <= right);
}

bool operator>=(const Vector& left, const Vector& right) {
  return left > right;
}

Vector operator+(const Vector& left, const Vector& right) {
  Vector data = left;
  data += right;
  return data;
}

Vector operator-(const Vector& left, const Vector& right) {
  Vector data = left;
  data -= right;
  return data;
}

bool operator==(const Vector& left, const Vector& right) {
  return fabsl(left.x - right.x) <= accuracy &&
         fabsl(left.y - right.y) <= accuracy;
}

bool operator!=(const Vector& left, const Vector& right) {
  return !(left == right);
}

long double OrientalSquare(const Vector& first, const Vector& second) {
  return first.x * second.y - first.y * second.x;
}

Vector FindMinVertice(std::vector<Vector>& vertices) {
  Vector min = vertices[0];
  for (auto& i : vertices) {
    if (i.x < min.x || (i.x == min.x && i.y < min.y)) {
      min = i;
    }
  }
  return min;
}

class Polygon {
  std::vector<Vector> vertices;

 public:
  Polygon() = default;
  Polygon(std::vector<Vector>& vertices) : vertices(vertices) {}
  long double Square() {
    long double res = 0;
    Vector begin = vertices[0];
    for (int i = 1; i < vertices.size() - 1; ++i) {
      res +=
          fabsl(OrientalSquare(vertices[i] - begin, vertices[i + 1] - begin));
    }
    return res;
  }
  friend Polygon MinkovskySum(const Polygon& a, const Polygon& b);
};

Polygon MinkovskySum(const Polygon& a, const Polygon& b) {
  Polygon res;
  std::vector<Vector> vectors;
  for (int i = 1; i < a.vertices.size() + 1; i++) {
    vectors.push_back(a.vertices[i % a.vertices.size()] - a.vertices[i - 1]);
  }
  for (int i = 1; i < b.vertices.size() + 1; i++) {
    vectors.push_back(b.vertices[i % b.vertices.size()] - b.vertices[i - 1]);
  }
  std::sort(vectors.begin(), vectors.end());
  res.vertices.emplace_back(0, 0);
  for (int i = 0; i < vectors.size() - 1; i++) {
    res.vertices.push_back(vectors[i] + res.vertices[res.vertices.size() - 1]);
  }
  return res;
}

long double countSquare(std::vector<Vector>& first,
                        std::vector<Vector>& second) {
  Polygon a(first), b(second);
  Polygon c = MinkovskySum(a, b);
  long double res = c.Square() - a.Square() - b.Square();
  res /= 4;
  return res;
}

int main() {
  std::cout.setf(std::ios::fixed);
  std::cout.precision(6);
  std::vector<Vector> vertices1;
  std::vector<Vector> vertices2;
  size_t n;
  std::cin >> n;
  for (size_t i = 0; i < n; i++) {
    long double x, y;
    std::cin >> x >> y;
    vertices1.emplace_back(x, y);
  }
  std::cin >> n;
  for (size_t i = 0; i < n; i++) {
    long double x, y;
    std::cin >> x >> y;
    vertices2.emplace_back(x, y);
  }
  long double res = countSquare(vertices1, vertices2);
  std::cout << res;
  return 0;
}
