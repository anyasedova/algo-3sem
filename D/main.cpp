#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

class StrClass {
private:
    size_t firstHalfValue;
    size_t secondHalfValue;
public:
    StrClass () = default;
    
    StrClass (size_t firstHalfValue, size_t secondHalfValue) : firstHalfValue(firstHalfValue),
    secondHalfValue(secondHalfValue) {}
    
    size_t getFirstHalf() const {
        return firstHalfValue;
    }
    
    size_t getSecondHalf() const {
        return secondHalfValue;
    }
    
    bool operator == (const StrClass& other) const {
        return other.firstHalfValue == firstHalfValue && other.secondHalfValue == secondHalfValue;
    }
    
    bool operator != (const StrClass& other) const {
        return !(*this == other);
    }
};

class SuffixArrayFinder {
private:
    static const char zero;
    std::vector<StrClass> classes;
    std::vector<size_t> suffixArray;
    std::string str;
    
    void sortStep(size_t lengthOfSubStr, size_t max);
    void firstStepSort ();
    void getNewClasses(size_t lengthOfSubStr);
public:
    explicit SuffixArrayFinder(std::string& _str);

    const std::vector<size_t>& getSuffixArray() {
        return suffixArray;
    }
};

const char SuffixArrayFinder::zero = 0;

void SuffixArrayFinder::sortStep(size_t lengthOfSubStr, size_t max) {
    
    std::vector<size_t> _suffixArray (suffixArray.size(), 0);
    std::vector<StrClass> _classes(classes.size());
    std::vector<size_t> countValuesBefore(max+1, 0);
    for (const auto & _curClass : classes) {
        ++countValuesBefore[_curClass.getFirstHalf()];
    }
    size_t numberOfValuesBefore = 0;
    for (size_t i = 0; i < countValuesBefore.size(); i++) {
        size_t cur = countValuesBefore[i];
        countValuesBefore[i] = numberOfValuesBefore;
        numberOfValuesBefore += cur;
    }
    
    for (size_t i = 0; i < classes.size(); i++) {
        _suffixArray[countValuesBefore[classes[i].getFirstHalf()]] = (str.size() + suffixArray[i] - lengthOfSubStr)%str.size();
        _classes[countValuesBefore[classes[i].getFirstHalf()]] = classes[i];
        ++countValuesBefore[classes[i].getFirstHalf()];
    }
    
    suffixArray = std::move(_suffixArray);
    classes = std::move(_classes);
    
}

void SuffixArrayFinder::firstStepSort() {
    std::vector<size_t> symbols(str.size(), 0);
    std::vector<int> _classes(str.size(), 0);
    
    for(size_t i = 0 ; i < str.size(); i++) {
        symbols[i] = i;
    }
    
    std::sort(symbols.begin(), symbols.end(), [this](size_t a, size_t b){
        return str[a] < str[b];
    });
    
    suffixArray.push_back(symbols[0]);
    
    int curClass = 0;
    for (size_t i = 1; i < symbols.size(); i++) {
        suffixArray.push_back(symbols[i]);
        if (str[symbols[i]] != str[symbols[i-1]]) {
            ++curClass;
        }
        _classes[symbols[i]] = curClass;
    }
    
    for (size_t curIndex: symbols) {
        classes.emplace_back(_classes[(curIndex + str.size() - 1) % str.size()], _classes[curIndex]);
    }
    
}

void SuffixArrayFinder::getNewClasses(size_t lengthOfSubStr) {
    std::vector<int> _classes(classes.size(), 0);
    int curClass = 0;
    for (size_t i = 1; i < classes.size(); i++) {
        if (classes[i] != classes[i-1]) {
            ++curClass;
        }
        _classes[suffixArray[i]] = curClass;
    }
    for (size_t i = 0; i < classes.size(); i++) {
        size_t curIndex = suffixArray[i];
        classes[i] = StrClass(_classes[(curIndex + str.size() - lengthOfSubStr) % str.size()], _classes[curIndex]);
    }
}

SuffixArrayFinder::SuffixArrayFinder(std::string & _str) : str(_str) {
    size_t origSize = str.size();
    str += zero;
    size_t _size = 1;
    while (str.size() > _size) {
        _size *= 2;
    }
    while (str.size() < _size) {
        str += (char) 0;
    }
    firstStepSort();
    size_t classesOldSize = 0;
    
    for (size_t i = 1; i < str.size(); i *= 2) {
        sortStep(i, classes[classes.size() - 1].getSecondHalf());
        getNewClasses(i * 2);
        
        if (classesOldSize == classes.size()) {
            break;
        }
        classesOldSize = classes.size();
    }
    
    suffixArray.erase(suffixArray.begin(), suffixArray.begin() + str.size() - origSize);
}

class LCPCounter {
private:
    std::string str;
    size_t sSize;
    std::vector<size_t> posInSuffixArray;
    std::vector<int> LCP;
public:
    
    explicit LCPCounter(std::string& str, const std::vector<size_t>& suffixArray)  : str(str), sSize(str.size()), LCP(str.size()), posInSuffixArray(str.size()) {
        countPos(suffixArray);
        countLCP(suffixArray);
    }
    
    void countPos(const std::vector<size_t> & suffixArray) {
        for(size_t i = 0; i < suffixArray.size(); i++) {
            posInSuffixArray[suffixArray[i]] = i;
        }
    }
    
    void countLCP (const std::vector<size_t>& suffixArray) {
        for (size_t i = 0; i < posInSuffixArray.size(); i++) {
            size_t curPos = posInSuffixArray[i];
            if (curPos == 0) {
                LCP[curPos] = 0;
            } else {
                size_t prevPos = i == 0 ? 1 : posInSuffixArray[i-1];
                LCP[curPos] = std::max(0, LCP[prevPos] - 1);
                size_t firstIndex = i + LCP[curPos], secondIndex = suffixArray[curPos - 1] + LCP[curPos];
                while(str[firstIndex] == str[secondIndex]) {
                    ++firstIndex;
                    ++secondIndex;
                    ++LCP[curPos];
                }
            }
        }
    }
    
    const std::vector<int> & getLCP() {
        return LCP;
    }
    
};

size_t countDiffSubstr(std::string& str) {
    SuffixArrayFinder suffArrFinder(str);
    LCPCounter LCPCounter(str, suffArrFinder.getSuffixArray());
    const std::vector<size_t> suffixArray = suffArrFinder.getSuffixArray();
    const std::vector<int> LCP =LCPCounter.getLCP();
    size_t num = 0;
    for (size_t i = 0; i < suffixArray.size(); i++) {
        num += str.size() - suffixArray[i] - LCP[i];
    }
    return num;
}

int main() {
    std::string str;
    std::cin >> str;
    std::cout << countDiffSubstr(str);
    return 0;
}
