#include <iostream>
#include <cmath>
#include <vector>
#include <assert.h>
#include <iomanip>
#include <algorithm>
#include <unordered_map>

class Vector {
 public:
  long long num;
  long long x;
  long long y;
  long long z;
  Vector() = default;
  Vector(long long x, long long y, long long z) : x(x), y(y), z(z) {}
  Vector(long long x, long long y, long long z, size_t num)
      : x(x), y(y), z(z), num(num) {}
  Vector(const Vector& vector) = default;

  Vector operator-() { return {-x, -y, -z}; }
  void operator+=(const Vector& other) {
    x += other.x;
    y += other.y;
    z += other.z;
  }
  void operator-=(const Vector& other) {
    x -= other.x;
    y -= other.y;
    z -= other.z;
  }
  void operator/=(const int& c) {
    assert(c > 0);
    x /= c;
    y /= c;
    z /= c;
  }
  void operator*=(const int& c) {
    x *= c;
    y *= c;
    z *= c;
  }
};

/*
 * Арифметические операции
 */

bool operator==(const Vector& left, const Vector& right);
bool operator!=(const Vector& left, const Vector& right);
Vector operator+(const Vector& left, const Vector& right);
bool operator<(const Vector& left, const Vector& right);
bool operator>(const Vector& left, const Vector& right);
bool operator>=(const Vector& left, const Vector& right);
bool operator<=(const Vector& left, const Vector& right);
Vector operator-(const Vector& left, const Vector& right);
Vector operator/(const Vector& left, const int& right);

Vector operator+(const Vector& left, const Vector& right) {
  Vector data = left;
  data += right;
  return data;
}
Vector operator-(const Vector& left, const Vector& right) {
  Vector data = left;
  data -= right;
  return data;
}
Vector operator/(const Vector& left, const int& right) {
  assert(right != 0);
  Vector data = left;
  data /= right;
  return data;
}
Vector operator*(const Vector& left, const long long right) {
  Vector data = left;
  data *= right;
  return data;
}

/*
 * Операции сравнения
 */
bool operator>(const Vector& left, const Vector& right) {
  return (left.x > right.x) && (left.y > right.y) && (left.z > right.z);
}
bool operator<(const Vector& left, const Vector& right) {
  return (left.x < right.x) && (left.y < right.y) && (left.z < right.z);
}
bool operator<=(const Vector& left, const Vector& right) {
  return (left.x <= right.x) && (left.y <= right.y) && (left.z <= right.z);
}
bool operator>=(const Vector& left, const Vector& right) {
  return (left.x >= right.x) && (left.y >= right.y) && (left.z >= right.z);
}

bool operator==(const Vector& left, const Vector& right) {
  return left.x == right.x && left.y == right.y;
}

bool operator!=(const Vector& left, const Vector& right) {
  return !(left == right);
}

long long scalarMul(const Vector& first, const Vector& second) {
  return first.x * second.x + first.y * second.y + first.z * second.z;
}

Vector vectorMul(const Vector& first, const Vector& second) {
  return Vector(first.y * second.z - first.z * second.y,
                first.z * second.x - first.x * second.z,
                first.x * second.y - first.y * second.x);
}

class Plane {
 public:
  Vector normal;
  long long D;
  Plane(const Vector& firstP, const Vector& secondP, const Vector& thirdP) {
    Vector firstV = firstP - thirdP, secondV = secondP - thirdP;
    normal = vectorMul(firstV, secondV);
    D = -scalarMul(normal, thirdP);
  }
  bool operator==(const Plane& another) const {
    Vector zero(0, 0, 0);
    return (vectorMul(normal, another.normal) == zero &&
            normal.x * another.D == another.normal.x * D);
  }
};

class Face {
 public:
  std::vector<Vector> vertices;
  Plane plane;
  Face(const std::vector<Vector>& vertices)
      : vertices(vertices), plane(vertices[0], vertices[1], vertices[2]) {
    Vector v1 = vertices[1] - vertices[0];
    Vector v2 = vertices[2] - vertices[1];
    if (scalarMul(vectorMul(v1, v2), plane.normal) < 0) {
      plane.normal = -plane.normal;
      plane.D = -plane.D;
    }
  }

  Face(const std::vector<Vector>& vertices1, Vector& inside)
      : vertices(vertices1), plane(vertices[0], vertices[1], vertices[2]) {
    if (IsViewedFrom(inside)) {
      std::reverse(vertices.begin(), vertices.end());
    }
    Vector v1 = vertices[1] - vertices[0];
    Vector v2 = vertices[2] - vertices[1];
    if (scalarMul(vectorMul(v1, v2), plane.normal) < 0) {
      plane.normal = -plane.normal;
      plane.D = -plane.D;
    }
  }

  long long IsViewedFrom(const Vector& x) const {
    if (scalarMul(plane.normal, x) + plane.D == 0) {
      return 0;
    } else {
      if (scalarMul(plane.normal, x) + plane.D > 0) {
        return 1;
      } else {
        return 0;
      }
    }
  }

  void Print() {
    std::cout << vertices.size() << ' ';
    for (long long i = 0; i < vertices.size(); ++i) {
      std::cout << vertices[i].num << ' ';
    }
    std::cout << std::endl;
  }

  void sort() {
    std::vector<Vector> a(vertices.size());
    long long min = 0;
    for (long long i = 0; i < vertices.size(); ++i) {
      if (vertices[i].num < vertices[min].num) {
        min = i;
      }
    }
    for (long long i = min; i < vertices.size() + min; ++i) {
      a[i - min] = vertices[(i) % vertices.size()];
    }
    vertices = std::move(a);
  }

  bool operator<(const Face& another) const {
    for (long long i = 0; i < vertices.size(); i++) {
      if (vertices[i].num < another.vertices[i].num) {
        return true;
      }
      if (vertices[i].num > another.vertices[i].num) {
        return false;
      }
    }
    return false;
  }

  bool operator==(const Face& another) const {
    const std::vector<Vector>& p1 = vertices;
    const std::vector<Vector>& p2 = another.vertices;
    return (p1[0].num == p2[0].num && p1[1].num == p2[1].num &&
            p1[2].num == p2[2].num) ||
           (p1[0].num == p2[1].num && p1[1].num == p2[2].num &&
            p1[2].num == p2[0].num) ||
           (p1[0].num == p2[2].num && p1[1].num == p2[0].num &&
            p1[2].num == p2[1].num) ||
           (p1[0].num == p2[2].num && p1[1].num == p2[1].num &&
            p1[2].num == p2[0].num) ||
           (p1[0].num == p2[1].num && p1[1].num == p2[0].num &&
            p1[2].num == p2[2].num) ||
           (p1[0].num == p2[0].num && p1[1].num == p2[2].num &&
            p1[2].num == p2[1].num);
  }
};
template <>
struct std::hash<Face> {
  std::size_t operator()(const Face& face) const {
    return std::hash<int>()(abs(face.plane.normal.x)) +
           std::hash<int>()(abs(face.plane.normal.y)) +
           std::hash<int>()(abs(face.plane.normal.z)) +
           std::hash<int>()(abs(face.plane.D));
  }
};

class ConvexHullFinder {
 public:
  std::unordered_map<Face, bool> ConvexHull;
  std::vector<Vector> vertices;

  void AddVertice(const Vector& x) {
    std::vector<Face> toDelete;
    for (auto& face : ConvexHull) {
      if (face.second) {
        if (face.first.IsViewedFrom(x)) {
          toDelete.emplace_back(face.first);
          ConvexHull[face.first] = false;
        }
      }
    }
    for (auto& face : toDelete) {
      for (long long i = 1; i < face.vertices.size() + 1; ++i) {
        Face faceToAdd(
            {face.vertices[i - 1], face.vertices[i % face.vertices.size()], x});
        if (ConvexHull.find(faceToAdd) != ConvexHull.end()) {
          ConvexHull.erase(faceToAdd);
        } else {
          ConvexHull[faceToAdd] = true;
        }
      }
      ConvexHull.erase(face);
    }
  }

  ConvexHullFinder(long long n) { Scan(n); }
  void Scan(long long n) {
    for (long long i = 0; i < n; i++) {
      long long x, y, z;
      std::cin >> x >> y >> z;
      vertices.emplace_back(x, y, z, i);
    }
    FindFirst();
    for (long long i = 4; i < vertices.size(); i++) {
      AddVertice(vertices[i]);
    }
  }

  void FindFirst() {
    Face x1({vertices[0], vertices[1], vertices[2]}, vertices[3]),
        x2({vertices[0], vertices[3], vertices[2]}, vertices[1]),
        x3({vertices[3], vertices[1], vertices[2]}, vertices[0]),
        x4({vertices[0], vertices[1], vertices[3]}, vertices[2]);
    ConvexHull[x1] = true;
    ConvexHull[x2] = true;
    ConvexHull[x3] = true;
    ConvexHull[x4] = true;
  }

  void Print() {
    std::vector<Face> res;
    for (auto i : ConvexHull) {
      res.push_back(i.first);
      res.back().sort();
    }
    std::cout << res.size() << std::endl;
    std::sort(res.begin(), res.end());
    for (auto& i : res) {
      i.Print();
    }
    std::cout << std::endl;
  }
};

int main() {
  long long n, m = 1;
  std::cin >> m;
  for (long long i = 0; i < m; i++) {
    std::cin >> n;
    ConvexHullFinder a(n);
    a.Print();
  }
  return 0;
}
