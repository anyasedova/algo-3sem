#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <deque>
#include <assert.h>

/*
 * считаем z-функцию для первых pattern.size() элементов
 */
 std::vector<size_t> CountZFuncForPattern(const std::string &pattern) {
    std::vector<size_t> zFunc;
    zFunc.push_back(0);
    size_t l = 0;
    size_t r = 0;
    for (size_t i = 1; i < pattern.size(); i++) {
        zFunc.push_back(r > i? std::min(zFunc[i-l], r-i) : 0);
        while (pattern[i+zFunc[i]] == pattern[zFunc[i]]) {
            zFunc[i]++;
        }
        if (i + zFunc[i] > r) {
            l = i;
            r = i + zFunc[i];
        }
    }
    return zFunc;
}

/*
 * считаем функцию в подстроке, в которой ищем шаблон
 */
void FindPositionOfPattern(const std::string &pattern, std::vector<size_t> &precountedZFunc) {
        size_t templateSize = precountedZFunc.size();
        size_t curZFunc  = 0;
        size_t l = 0;
        size_t r = 0;
        std::deque<char> curS;
        curS.push_back('$');
        char next = (char)std::cin.get();
        next = '$';
        size_t i = templateSize+ 1;
        for (size_t j = 0; j < pattern.size() - 1; ++j) {
            if ((next = (char)std::cin.get()) == EOF || next == '\n'){
                return;
            }
            curS.push_back(next);
        }
        while ((next = (char)std::cin.get()) != EOF && next != '\n') {
            assert(i-l >= 0);
            curS.pop_front();
            curS.push_back(next);
            curZFunc = r > i? std::min(precountedZFunc[i-l], r-i) : 0;
            while (curZFunc < pattern.size() && curS[curZFunc] == pattern[curZFunc]) {
                curZFunc++;
            }
            if (i + curZFunc > r) {
                l = i;
                r = i + curZFunc;
            }
            if (curZFunc == templateSize) {
                std::cout << i - templateSize - 1 << ' ';
            }
            ++i;
        }
}

void findPattern (const std::string &pattern) {
    std::vector<size_t> precountedZ;
    /*
     * оцениваем z-функцию как размер шаблона (больше не может быть, так как разделяющий символ один)
     */
    precountedZ = CountZFuncForPattern(pattern);
    FindPositionOfPattern(pattern, precountedZ);
}

int main() {
    std::cin.tie(NULL);
    std::string t;
    std::cin >>t;
    findPattern(t);
    return 0;
}
