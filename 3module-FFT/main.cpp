
#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>

double pi = 3.141592653589793238;

void doFFT(std::vector<std::complex<double>>& values, bool isReversed) {
    if (values.size() == 1) {
        return;
    }
    std::vector<std::complex<double>> oddDegree, evenDegree;
    for (size_t i = 0; i < values.size(); i+=2) {
        evenDegree.push_back(values[i]);
    }
    for (size_t i = 1; i < values.size(); i+=2) {
        oddDegree.push_back(values[i]);
    }
    
    doFFT(evenDegree, isReversed);
    doFFT(oddDegree, isReversed);
    
    double ang = isReversed? -2*pi/values.size() : 2*pi/values.size();
    std::complex<double> root(cos(ang), sin(ang)), cur(1);
    for (size_t i = 0; i < values.size()/2; i++) {
        values[i] = evenDegree[i] + cur * oddDegree[i];
        values[i + values.size()/2] =evenDegree[i] - cur * oddDegree[i];
        cur *= root;
    }
    
    if (isReversed) {
        for(auto& value : values) {
            value/=2;
        }
    }
}


std::vector<std::complex<double>> Sum (const std::vector<std::complex<double>> &first,
                                       const std::vector<std::complex<double>> &second) {
    std::vector<std::complex<double>> res(first.size());
    for (size_t i = 0; i < first.size(); i++) {
        res[i] = first[i] + second[i];
    }
    return res;
}

std::vector<std::complex<double>> MultiplyValues (const std::vector<std::complex<double>> &                                 first, const std::vector<std::complex<double>> &second){
    std::vector<std::complex<double>> res(first.size());
    for (size_t i = 0; i < first.size(); i++) {
        res[i] = first[i] * second[i];
    }
    return res;
}

void Read (size_t size, std::vector<std::complex<double>> &a, std::vector<std::complex<double>> &t, std::vector<std::complex<double>> &g, std::vector<std::complex<double>> &c) {
    for (int i = 0; i < size; i++) {
        char x;
        std::cin >> x;
        if (x == 'A') {
            a[i] = 1;
            t[i] = 0;
            g[i] = 0;
            c[i] = 0;
        }
        if (x == 'T') {
            a[i] = 0;
            t[i] = 1;
            g[i] = 0;
            c[i] = 0;
        }
        if (x == 'G') {
            a[i] = 0;
            t[i] = 0;
            g[i] = 1;
            c[i] = 0;
        }
        if (x == 'C') {
            a[i] = 0;
            t[i] = 0;
            g[i] = 0;
            c[i] = 1;
        }
    }
}

void Print(const std::vector<std::complex<double>> & v) {
    for(auto i: v) {
        std::cout << i;
    }
    std::cout << std::endl;
}

size_t FindMax(const std::vector<std::complex<double>> &v) {
    size_t answ = 0;
    for (size_t i = 0;i < v.size(); i++) {
        if (v[answ].real() < v[i].real()){
            answ = i;
        }
    }
    return answ;
}

void Solve() {
    size_t n;
    std::cin >> n;
    std::vector<std::complex<double>> a1(n), t1(n), g1(n), c1(n),
    a2(n), t2(n), g2(n), c2(n);
    Read(n, a1, t1, g1, c1);
    Read(n, a2, t2, g2, c2);
    std::reverse(a2.begin()+1, a2.end());
    std::reverse(t2.begin()+1, t2.end());
    std::reverse(g2.begin()+1, g2.end());
    std::reverse(c2.begin()+1, c2.end());
    doFFT(a1, false);
    doFFT(t1, false);
    doFFT(c1, false);
    doFFT(g1, false);
    doFFT(a2, false);
    doFFT(t2, false);
    doFFT(c2, false);
    doFFT(g2, false);
    std::vector<std::complex<double>> result = Sum(Sum(MultiplyValues(a1, a2), MultiplyValues(t1, t2)), Sum(MultiplyValues(c1, c2), MultiplyValues(g1, g2)));
    doFFT(result, true);
    size_t answ = FindMax(result);
    std::cout << lround(result[answ].real()) << ' ' << answ << std::endl;
}

int main() {
    Solve();
    return 0;
}
