//
//  main.cpp
//  C
//
//  Created by Аня Седова on 10.12.2020.
//

#include <iostream>
#include <vector>
#include <set>

size_t mex (const std::set <size_t>& s) {
    size_t min = 0;
    while(s.count(min)) {
        ++min;
    }
    return min;
}


int main() {
    size_t n;
    std::cin >> n;
    std::vector<size_t> SG(n > 2? n + 1 : 3, false);
    SG[1] = 0;
    SG[2] = 1;
    for (size_t i = 3; i < n; i++) {
        std::set<size_t> cur;
        cur.insert(SG[i-1]);
        cur.insert(SG[i-2]);
        for (size_t j = 3; j <= i-2; ++j) {
            cur.insert(SG[j - 1] ^ SG[i - j]);
        }
        SG[i] = mex(cur);
    }
    
    std::vector<size_t> winPositions;
    
    if (SG[n-1] == 0) {
        winPositions.push_back(1);
    }
    if (SG[n-2] == 0) {
        winPositions.push_back(2);
    }
    
    for (size_t j = 3; j <= n-2; ++j) {
        if ((SG[j - 1] ^ SG[n - j]) == 0) {
            winPositions.push_back(j);
        }
    }
    if (SG[n-2] == 0 && (n-1 > 2)) {
       winPositions.push_back(n-1);
    }
    if (SG[n-1] == 0 && (n > 2)) {
        winPositions.push_back(n);
    }
    if (winPositions.size() == 0) {
        std::cout << "Mueller";
    } else {
        std::cout << "Schtirlitz\n";
        for (auto pos:winPositions) {
            std::cout << pos << std::endl;
        }
    }
    return 0;
}
