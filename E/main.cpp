#include <iostream>
#include <set>
#include <vector>
#include <assert.h>

class Point {
public:
    size_t segmentNum;
    bool isEnd;
    long long x;
    long long y;
    
    
    Point(long long x,long long y, size_t num, bool isEnd) : segmentNum(num), x(x), y(y), isEnd(isEnd){}
    
    friend bool operator < (const Point& first, const Point& second);
    friend bool operator == (const Point& first, const Point& second);
    friend bool operator != (const Point& first, const Point& second);
    
    bool IsBeside(const Point& f, const Point& s) const {
        return (f.x >= x && x >= s.x) || ((s.x >= x && x >= f.x));
    }
};

bool operator < (const Point& first, const Point& second){
    return (first.x < second.x) || ((first.x == second.x) && (first.y < second.y))
    || (first.x == second.x && first.y == second.y && first.isEnd < second.isEnd);
}

bool operator ==(const Point& first, const Point& second){
    return (first.y == second.y) && (first.x == second.x);
}

bool operator !=(const Point& first, const Point& second){
    return !(first==second);
}

class Segment {
public:
    size_t num;
    Point begin;
    Point end;
    
    Segment(const Point& b, const Point& e, size_t num) :num(num), begin(b), end(e){
        if (end < begin) {
            std::swap(begin, end);
            begin.isEnd = false;
            end.isEnd = true;
        }
    }
    
    long double getY (long long x) const {
        if (end.x == begin.x) {
            return begin.y;
        }
        return (long double)begin.y +
        ((long double)(end.y - begin.y) *(long double)(x - begin.x))/(long double)(end.x - begin.x);
    }
    
    friend bool operator < (const Segment& first, const Segment& second);
};

bool operator < (const Segment& first, const Segment& second){
    long long x = std::max(std::min(first.begin.x,first.end.x), std::min(second.begin.x, second.end.x));
    return first.getY(x) < second.getY(x) - 1E-9;
}

bool IsSameSign(long long x, long long y) {
    return (x >0 && y > 0) || (x <0 && y <0);
}

bool IsIntersect(const Segment& f, const Segment& s) {
    
    if (f.begin == f.end) {
        if (f.begin == s.begin || f.begin == s.end) {
            return 1;
        } else {
            return 0;
        }
    }
    
    if (s.begin == s.end) {
        if (f.begin == s.begin || s.begin == f.end) {
            return 1;
        } else {
            return 0;
        }
    }
    
    if (f.begin == s.end || f.begin == s.begin || f.end == s.begin || f.end == s.end) {
        return true;
    }
    
    
    long long s1 = (f.begin.x - f.end.x)*(s.begin.y - f.end.y) - (f.begin.y - f.end.y)*(s.begin.x - f.end.x);
    long long s2 = (f.begin.x - f.end.x)*(s.end.y - f.end.y) - (f.begin.y - f.end.y)*(s.end.x - f.end.x);
    long long s3 = (s.begin.x - s.end.x)*(f.end.y - s.end.y) - (s.begin.y - s.end.y)*(f.end.x - s.end.x);
    long long s4 = (s.begin.x - s.end.x)*(f.begin.y - s.end.y) - (s.begin.y - s.end.y)*(f.begin.x - s.end.x);
    
    
    if (s1 == 0 && s2 == 0) {
        return s.begin.IsBeside(f.begin, f.end) || s.end.IsBeside(f.begin, f.end)
        || f.begin.IsBeside(s.begin, s.end) || f.end.IsBeside(s.begin, s.end);
    }
    
    if (s1 == 0) {
        return s.begin.IsBeside(f.begin, f.end);
    }
    
    if (s2 == 0) {
        return s.end.IsBeside(f.begin, f.end);
    }
    
    if (s3 == 0) {
        return f.end.IsBeside(s.begin, s.end);
    }
    
    if (s4 == 0) {
        return f.end.IsBeside(s.begin, s.end);
    }
    
    return !IsSameSign(s1, s2) && !IsSameSign(s3, s4);
}


void FindIntersection (std::set<Point>& Q, std::vector<Segment> & segments) {
    std::set<Segment> status;
    std::vector<std::set<Segment>::iterator> pos(segments.size());
    
    for(auto it = Q.begin(); it != Q.end(); ++it){
        size_t curPos = it->segmentNum;
        
        if (it->isEnd) {
            auto curIt = pos[curPos];
            auto next = std::next(curIt);
            auto prev = curIt != status.begin() ? std::prev(curIt) : status.end();
            
            if (prev != status.end() && next != status.end() && IsIntersect(*prev, *next)) {
                std::cout << "YES" << std::endl << next->num + 1 << ' ' << prev->num + 1;
                return;
            }
            
            status.erase(curIt);
        } else {
            auto next = status.lower_bound(segments[curPos]);
            auto prev = next != status.begin() ? std::prev(next) : status.end();
            
            if (next != status.end() && IsIntersect(*next, segments[curPos])) {
                std::cout << "YES" << std::endl << next->num + 1 << ' ' << curPos+ 1;
                return;
            }
            
            if (prev != status.end() && IsIntersect(*prev, segments[curPos])) {
                std::cout << "YES" << std::endl << prev->num + 1 << ' ' << curPos+ 1;
                return;
            }
            
            pos[curPos] = status.insert(next, segments[curPos]);
        }
    }
    std::cout << "NO";
    return;
}


int main() {
    int n;
    std::cin >> n;
    std::vector<Segment> segments;
    std::set<Point> points;
    for (size_t i = 0; i < n; i++) {
        int x, y, z,t;
        std::cin >> x >> y >> z >> t;
        segments.emplace_back(Point(x, y, i, false), Point(z, t, i, true), i);
        Point f = segments.back().begin;
        Point s = segments.back().end;
        
        if (points.count(f) > 0) {
            std::cout << "YES" << std::endl << points.find(f)->segmentNum + 1 << ' ' << i+1;
            return 0;
        }
        
        if (points.count(s) > 0) {
            std::cout << "YES" << std::endl << points.find(s)->segmentNum + 1 << ' ' << i+1;
            return 0;
        }
        
        points.insert(f);
        points.insert(s);
    }
    FindIntersection(points, segments);
    return 0;
}
